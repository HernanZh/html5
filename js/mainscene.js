//main game
//
MainScene.prototype = new Scene;
MainScene.prototype.constructor = MainScene;
function MainScene(parent)
{
	this.parent=parent;
	//for optimization: we dont include inactive tiles so there's less loops
	this.init=function()
	{
		//this.instances.push(new Ball(this));
		//this.instances.push(new PlayerInstance(this,64,64));
		//this.instances.push(new EnemyInstance(this,160,160));
		
		//we make an array create a tilemap
		//for bigger projects: parse an external file
		var tiles= [1,2,2,2,2,2,2,2,3,
					5,6,6,6,6,6,6,6,7,
					5,6,6,4,6,6,6,6,7,
					5,6,6,6,6,6,6,6,7,
					5,6,6,6,8,6,6,6,7,
					5,8,6,6,6,6,6,8,7,
					5,6,4,6,6,6,6,6,7,
					5,6,6,6,6,4,6,6,7,
					5,6,6,6,6,6,6,6,7,
					9,10,10,10,10,10,10,10,11];
		var objects= [0,0,0,0,0,0,0,0,0,
					0,0,0,0,0,0,0,0,0,
					0,0,0,0,0,0,0,0,0,
					0,0,0,0,0,0,0,0,0,
					0,0,0,0,0,0,0,0,0,
					0,0,0,0,0,0,0,0,0,
					0,0,0,0,0,0,0,0,0,
					0,0,0,0,0,0,0,0,0,
					0,0,0,0,0,0,0,0,0,
					0,0,0,0,0,0,0,0,0];
		
		//put some random blocks, place player and enemy at random place
		for (var i=0;i<5;i++)
		{
			var x=getRandom(7)+1;
			var y=getRandom(8)+1;
			var n=x+y*9;
			objects[n]=1;
		}
		{
			var x=getRandom(5)+2;
			var y=getRandom(6)+2;
			var n=x+y*9;
			objects[n]=2;		
		}
		{
			var x=getRandom(7)+1;
			var y=getRandom(8)+1;
			var n=x+y*9;
			objects[n]=3;		
		}
		
		for (var i=0;i<tiles.length;i++)
		{
			var solid=false;
			if (tiles[i] == 1 || tiles[i] == 3 || tiles[i] == 9 || tiles[i] == 11 || tiles[i] == 2 || tiles[i] == 5 || tiles[i] == 7 || tiles[i] == 10)
			{
				solid=true;
				this.instances.push(new BackgroundInstance(this,i%9*32,Math.floor(i/9)*32,this.parent.images.ground,tiles[i],solid));
			}
			else if (tiles[i] == 4 || tiles[i] == 8)
			{
				//we are gonna ignore tile 6, drawing too many tiles impacts speed greatly on mobile
				this.instances.push(new BackgroundInstance(this,i%9*32,Math.floor(i/9)*32,this.parent.images.ground,tiles[i],solid));
			}
			if (objects[i]==1)
				this.instances.push(new BlockInstance(this,i%9*32,Math.floor(i/9)*32+32));
			else if (objects[i]==2)
				this.instances.push(new PlayerInstance(this,i%9*32+16,Math.floor(i/9)*32+32));
			else if (objects[i]==3)
				this.instances.push(new EnemyInstance(this,i%9*32+12,Math.floor(i/9)*32+32-12));
			
		}
	}
	this.init();
	
	this.screenOffsetX=0;
	this.screenOffsetY=0;
	this.screenShakeTimer=0;
	this.screenShakeStrength=0;
	this.screenShake=function(duration,strength)
	{
		this.screenShakeTimer=duration;
		this.screenShakeStrength=strength;
	}
	this.clearTime=0;
	this.time="";
}

MainScene.prototype.draw = function(context)
{
	context.fillStyle = "#4B5259";
	context.fillRect(16, 16, this.parent.canvas.width-32, this.parent.canvas.height-32);

	//also sort instances by y value
	this.instances.sort(function(a,b){return ( b.z==a.z ? -b.y+a.y : -b.z+a.z ) })	
	for (var i=0;i<this.instances.length;i++)
	{
		context.translate(this.screenOffsetX,this.screenOffsetY);
		this.instances[i].draw(context);
		context.setTransform(1,0,0,1,0,0);
	}
	
	//show time
	context.fillStyle = "white";
	var msTotal=Math.floor(this.clearTime*1000/60);
	var ms=msTotal%1000;
	var seconds=Math.floor(msTotal/1000)%60;
	var minutes=Math.floor(msTotal/1000/60);
	this.time=""+(minutes>=10 ? minutes : "0"+minutes )+":"+(seconds>=10 ? seconds : "0"+seconds )+":"+(ms>=100? ms : (ms>=10? "0"+ms : "00"+ms));
	context.fillText("Time: "+this.time, 10,10);

}

MainScene.prototype.update = function()
{
	this.timer++;
	var destroy=new Array();
	//update all instances
	for (var i=0;i<this.instances.length;i++)
	{
		this.instances[i].update();
		//mark for deletion
		if (this.instances[i].destroy)
			destroy.push(i);
	}
	while (destroy.length>0)
	{
		this.instances.splice(destroy.pop(),1);
	}
	
	if (this.screenShakeTimer>0)
	{
		this.screenShakeTimer--;
		this.screenOffsetX=getRandom(this.screenShakeStrength)-this.screenShakeStrength/2;
		this.screenOffsetY=getRandom(this.screenShakeStrength)-this.screenShakeStrength/2;
	}
	if (this.screenShakeTimer==0)
	{
		this.screenOffsetX=0;
		this.screenOffsetY=0;	
	}
	//win condition: all enemies dead
	var nEnemies=0;
	for (var i=0;i<this.instances.length;i++)
	{
		if (this.instances[i] instanceof EnemyInstance)
			nEnemies++;
	}
	if (nEnemies==0)
	{
		alert("WIN WIN\n"+"Your score: "+this.time);
		if (this.clearTime < highscore || highscore==0)
			highscore=this.clearTime;
		
		this.parent.endScene(0);
	}
}
