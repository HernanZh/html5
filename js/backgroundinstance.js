BackgroundInstance.prototype = new Instance;
BackgroundInstance.prototype.constructor = BackgroundInstance;
function BackgroundInstance(parent,x,y,image,n,solid)
{
	this.parent=parent;
	this.x=x;
	this.y=y;
	this.z=-1;
	this.image=image;
	this.solid=solid;
	this.imX=(n%(this.image.width/32))*32;
	this.imY=Math.floor(n/(this.image.width/32))*32;
	this.hitbox={'x':0,'y':0,'w':32,'h':32};
}
BackgroundInstance.prototype.draw = function(context)
{
	context.drawImage(this.image, this.imX, this.imY, 32, 32,this.x,this.y,32,32);
}
BackgroundInstance.prototype.update = function()
{
}
