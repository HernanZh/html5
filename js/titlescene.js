TitleScene.prototype = new Scene;
TitleScene.prototype.constructor = TitleScene;
function TitleScene(parent)
{
	this.parent=parent;
	this.init=function()
	{
		this.instances.push(new ButtonInstance(this));
	}
	this.init();
}

TitleScene.prototype.draw = function(context)
{
	for (var i=0;i<this.instances.length;i++)
	{
		this.instances[i].draw(context);
	}
	
	//write some text
	context.fillStyle = "white";
	context.fillText("HIGHSCORE: ", 10,10);
	var msTotal=Math.floor(highscore*1000/60);
	var ms=msTotal%1000;
	var seconds=Math.floor(msTotal/1000)%60;
	var minutes=Math.floor(msTotal/1000/60);
	context.fillText("HIGHSCORE: "+(minutes>=10 ? minutes : "0"+minutes )+":"+(seconds>=10 ? seconds : "0"+seconds )+":"+
	(ms>=100? ms : (ms>=10? "0"+ms : "00"+ms)), 10,10);
	
	context.fillText("HOW TO PLAY:", 10,230);
	context.fillText("Keyboard", 10,250);
	context.fillText("   Arrow keys: Move", 10,260);
	context.fillText("   Space: Shoot", 10,270);
	context.fillText("Touch screen", 10,290);
	context.fillText("   Swipe: Move", 10,300);
	context.fillText("   Tap: Shoot", 10,310);

}