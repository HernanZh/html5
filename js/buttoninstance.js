ButtonInstance.prototype = new Instance;
ButtonInstance.prototype.constructor = ButtonInstance;
function ButtonInstance(parent)
{
	this.parent=parent;
	this.gameParent=parent.parent;
	this.x=CANVAS_WIDTH/2-100;
	this.y=CANVAS_HEIGHT/2-20;
	this.hitbox={'x':0,'y':0,'w':200,'h':40};
	this.setSprite(this.gameParent.sprites.button);
}

ButtonInstance.prototype.update = function()
{
	//click the button to continue
	if (this.gameParent.inputManager.mouseHold==1 
		&& this.gameParent.inputManager.lastMouseX > this.x
		&& this.gameParent.inputManager.lastMouseX < this.x+200
		&& this.gameParent.inputManager.lastMouseY > this.y-20
		&& this.gameParent.inputManager.lastMouseY < this.y+60
		)
	{
		this.gameParent.endScene(1);
	}
}