//Instance base class
//These are any objects that can be interacted within the game
//Equivalent of an "Object" in GameMaker, this is why it also contains sprite specific variables such as scale and angle 
function Instance(parent)
{	
	//parent must always be a scene object
	this.parent=parent;
	this.x=0;
	this.y=0;
	this.z=0;
	this.hitbox={'x':0,'y':0,'w':0,'h':0};
	this.imageSpeed=1.0;
	this.scale={'x':1,'y':1};
	this.angle=0;
	this.sprite;
	this.spriteReference;
	this.destroy=false;
	this.setSprite=function(sprite)
	{
		if (this.spriteReference!=sprite)
		{
			this.spriteReference=sprite;
			//we copy a new sprite here so other instances can reuse it
			this.sprite=new Sprite(sprite.image,sprite.animation,sprite.center);
			this.sprite.parent=this;
		}
	}
	this.isColliding=function(x,y,other)
	{
		if (this==other)
			return false;
		if(x+this.hitbox.x+this.hitbox.w <= other.x+other.hitbox.x)  //left
			return false;
		else if(y+this.hitbox.y+this.hitbox.h <= other.y+other.hitbox.y) //upper
			return false;
		else if (x+this.hitbox.x >= other.x+other.hitbox.x+other.hitbox.w) //right
			return false;
		else if(y+this.hitbox.y >= other.y+other.hitbox.y+other.hitbox.h) //lower
			return false;
		else
			return true;

	}
}
Instance.prototype.draw = function(context)
{
	//draw sprite
	if (this.sprite)
		this.sprite.draw(context);
}
Instance.prototype.update = function()
{
	//update sprite
	if (this.sprite)
		this.sprite.update();
}
Instance.prototype.animationEndCallback = function()
{	
}



//====================
//this obejct is for testing purposes
DebugInstance.prototype = new Instance;
DebugInstance.prototype.constructor = DebugInstance;
function DebugInstance(parent)
{
	this.gameParent=parent.parent;
	this.x=160;
	this.y=80;
	this.hitbox={'x':0,'y':0,'w':0,'h':0};
	this.setSprite(this.gameParent.sprites.bunnyStopped);
	this.maxSpeed=2.0;
}
DebugInstance.prototype.draw = function(context)
{
	if (!this.gameParent)
	{//debugging
		console.trace();
	}
	if (this.gameParent.inputManager.mouseHold > 1)
	{
		//draw circle representing virtual stick
		context.fillStyle = "white";
		context.beginPath();
		var dist=10;
		context.arc(this.x+this.speedX*dist,this.y+this.speedY*dist,5,0,2*Math.PI);
		context.closePath();
		context.fill();
		
		if (this.speed > 0.0)
		{
			this.setSprite(this.gameParent.sprites.bunnyWalking);
			//variable animation speed
			this.imageSpeed=Math.max(this.speed/(this.maxSpeed*this.maxSpeed)*0.25,0.15);
			//flip when walking left
			this.scale.x=this.speedX >= 0 ? 1 : -1;
		}
		else
			this.setSprite(this.gameParent.sprites.bunnyStopped);
	
	}
	else
		this.setSprite(this.gameParent.sprites.bunnyStopped);
	
	this.sprite.draw(context);


}
DebugInstance.prototype.update = function()
{
	this.sprite.update();
	
	//moving: using the mouse as a virtual joystick
	if (this.gameParent.inputManager.mouseHold == 1)
	{
		this.mouseX=this.gameParent.inputManager.lastMouseX;
		this.mouseY=this.gameParent.inputManager.lastMouseY;
		this.startMouseX=this.gameParent.inputManager.lastMouseX;
		this.startMouseY=this.gameParent.inputManager.lastMouseY;
	}
	if (this.gameParent.inputManager.mouseHold > 0)
	{
		var distanceDiv=10.0;
		this.mouseX+=this.gameParent.inputManager.deltaX;
		this.mouseY+=this.gameParent.inputManager.deltaY;
		this.speedX=(this.mouseX-this.startMouseX)/distanceDiv;
		this.speedY=(this.mouseY-this.startMouseY)/distanceDiv;
		this.speed=this.speedX*this.speedX+this.speedY*this.speedY;
		if (this.speed > this.maxSpeed*this.maxSpeed)
		{
			var angle=Math.atan2(this.speedX,this.speedY);
			this.speedX=this.maxSpeed*Math.sin(angle);
			this.speedY=this.maxSpeed*Math.cos(angle);
			this.mouseX=this.startMouseX+this.speedX*distanceDiv;
			this.mouseY=this.startMouseY+this.speedY*distanceDiv;
		}
		
		this.x+=this.speedX;
		this.y+=this.speedY;
	}
	//boundary
	if (this.x < 0)
		this.x=0;
	else if (this.x > CANVAS_WIDTH)
		this.x=CANVAS_WIDTH;
	if (this.y < 0)
		this.y=0;
	else if (this.y > CANVAS_HEIGHT)
		this.y=CANVAS_HEIGHT;

}