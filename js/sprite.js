//Sprite class
//Besides drawing an image, its also a simple animation class.
//The input paramater "animation" is a 2d array holding x and y coordinates, width and height
//to play an animation from a spritesheet. Normally I would save image/animation definitions
//in external files but considering the scope of this project I will do this programmatically.
function Sprite(image,animation,center)
{
	//parent must be a Instance object, always use Instance.setSprite
	this.parent=null;
	this.image=image;
	this.currentFrame=0.0;
	this.animation=animation;
	if (center === undefined)
		this.center={'x':0,'y':0};
	else
		this.center=center;
		
	this.draw=function(context)
	{
		
		//grab the rectangle from image defined by the animation 
		var cf=Math.floor(this.currentFrame);
		var sx=animation[cf][0];
		var sy=animation[cf][1];
		var sw=animation[cf][2];
		var sh=animation[cf][3];
		
		//set transformation matrix to scale and rotate
		context.translate(Math.floor(this.parent.x-this.center.x*this.parent.scale.x),Math.floor(this.parent.y-this.center.y*this.parent.scale.y));
		//scale
		context.scale(this.parent.scale.x,this.parent.scale.y);
		//rotation
		context.translate(Math.floor(this.center.x),Math.floor(this.center.y));
		if (this.parent.angle!=0)
			context.rotate(this.parent.angle);
		context.translate(Math.floor(-this.center.x),Math.floor(-this.center.y));
		//draw the image!
		context.drawImage(this.image, sx, sy, sw, sh,0,0,sw,sh);
		//reset to identity matrix
		context.setTransform(1,0,0,1,0,0);
	}
	this.update=function()
	{
		this.currentFrame+=this.parent.imageSpeed;
		var reachedEnd=false;
		while (this.currentFrame >= this.animation.length)
		{
			this.currentFrame -= this.animation.length;
			reachedEnd=true;
		}
		if (reachedEnd)
			this.parent.animationEndCallback();
	}
}