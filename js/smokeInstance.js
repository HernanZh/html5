SmokeInstance.prototype = new Instance;
SmokeInstance.prototype.constructor = SmokeInstance;
function SmokeInstance(parent,x,y,speedX,speedY,angle)
{
	this.parent=parent;
	this.gameParent=parent.parent;
	this.x=x;
	this.y=y;
	this.z=1;
	this.angle=angle;
	this.setSprite(this.gameParent.sprites.smoke);
	this.imageSpeed=0.2;
	this.speedX=speedX;
	this.speedY=speedY;
}
SmokeInstance.prototype.animationEndCallback=function()
{
	this.destroy=true;
}
SmokeInstance.prototype.update=function()
{
	this.sprite.update();
	
	this.x+=this.speedX;
	this.y+=this.speedY;
}