//Scene class
//A scene objects holds a collection of instances and updates them, for a bigger project
//the scene class would be more basic and should read a tilemap or level setup from an external file
//Equivalant to a "Room" in GameMaker
function Scene(parent)
{	
	//parent must be a Game object
	this.parent=parent;
	this.run=true;
	this.timer=0;
	this.instances=new Array();
	
	this.init=function()
	{		
		//do whatever here to start the scene
	}
	this.init();
	this.end=function()
	{
		//clean up objects
		while(this.instances.length>0)
			this.instances.pop();
	}
}
Scene.prototype.draw = function(context)
{
	//sort before drawing
	this.instances.sort(function(a,b){return a.z-b.z})
	//draw all instances
	for (var i=0;i<this.instances.length;i++)
	{
		this.instances[i].draw(context);
	}
}
Scene.prototype.update = function()
{
	this.timer++;
	var destroy=new Array();
	//update all instances
	for (var i=0;i<this.instances.length;i++)
	{
		this.instances[i].update();
		//mark for deletion
		if (this.instances[i].destroy)
			destroy.push(i);
	}
	while (destroy.length>0)
	{
		this.instances.splice(destroy.pop(),1);
	}
}

