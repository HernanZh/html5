Ball.prototype = new Instance;
Ball.prototype.constructor = Ball;
function Ball(parent)
{
	this.parent=parent;
	this.gameParent=parent.parent;
	this.radius=32;
	this.gravity=0.2;
	this.speedx=0;
	this.speedy=0;
	this.x=CANVAS_WIDTH/2;
	this.y=CANVAS_HEIGHT/2;;
	this.grabbed=false;
	this.color="black";
	
	this.isTouching=function (pointx,pointy)
	{
		var distance=(pointx-this.x)*(pointx-this.x)+(pointy-this.y)*(pointy-this.y);
		if (distance <= this.radius*this.radius)
			return true;
		else
			return false;
	}
}

Ball.prototype.draw=function(context)
{
	context.fillStyle = this.color;
	context.beginPath();
	context.arc(this.x,this.y,this.radius,0,2*Math.PI);
	context.fill();
		
	//show trajectory
	if (this.grabbed)
	{	
		//mirror mouse pos
		var xx=this.x+(this.x-this.gameParent.inputManager.lastMouseX);
		var yy=this.y+(this.y-this.gameParent.inputManager.lastMouseY);
		//get angle
		var angle=Math.atan2(this.x-xx,this.y-yy);
		
		//draw line
		context.strokeStyle="white";
		context.lineWidth=3.0;
		context.beginPath();
		context.moveTo(this.x,this.y);
		context.lineTo(xx,yy);
		context.stroke();
		
		//draw triangle
		context.fillStyle="white";
		context.beginPath();
		context.moveTo(xx,yy);
		context.lineTo(xx+20*Math.cos(angle-2*Math.PI/3.0),yy-20*Math.sin(angle-2*Math.PI/3.0));
		context.lineTo(xx+20*Math.cos(angle-2*Math.PI/6.0),yy-20*Math.sin(angle-2*Math.PI/6.0));
		context.closePath();
		context.fill();
	}
}

Ball.prototype.update = function()
{
	//physics
	if (!this.grabbed)
	{
		//vertical walls
		if (this.y+this.radius > CANVAS_HEIGHT)
		{
			this.y=CANVAS_HEIGHT-this.radius;
			this.speedy*=-0.5;
			this.speedx*=0.75;
		}
		else if (this.y-this.radius < 0)
		{
			this.y=this.radius;
			this.speedy*=-0.5;
			this.speedx*=0.75;
		}
		else
		{
			this.speedy+=this.gravity;
		}
					
		//horizontal walls
		if (this.x-this.radius < 0)
		{
			this.x=this.radius;
			this.speedx*=-0.75;
		}
		else if (this.x+this.radius > CANVAS_WIDTH)
		{
			this.x=CANVAS_WIDTH-this.radius;
			this.speedx*=-0.75;
		}		
		
		this.x+=this.speedx;
		this.y+=this.speedy;
	}
	//grab ball
	if (!this.grabbed && this.gameParent.inputManager.mouseHold == 1 && this.isTouching(this.gameParent.inputManager.lastMouseX,this.gameParent.inputManager.lastMouseY))
	{
		this.grabbed=true;
	}
	
	//release
	if (!this.gameParent.inputManager.mouseDown && this.grabbed)
	{
		this.grabbed=false;
		//shoot
		this.speedx=(this.x-this.gameParent.inputManager.lastMouseX)/5.0;
		this.speedy=(this.y-this.gameParent.inputManager.lastMouseY)/5.0;	
	}
}