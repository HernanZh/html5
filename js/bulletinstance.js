BulletInstance.prototype = new Instance;
BulletInstance.prototype.constructor = BulletInstance;
function BulletInstance(parent,x,y)
{
	this.parent=parent;
	this.gameParent=parent.parent;
	this.x=x;
	this.y=y;
	this.hitbox={'x':-4,'y':-4,'w':8,'h':8};
	this.setSprite(this.gameParent.sprites.bullet);
	this.speed=6.0;
	this.direction=0;
}

BulletInstance.prototype.update = function()
{
	if (this.direction==0)
		this.y+=this.speed;
	else if (this.direction==1)
		this.x+=this.speed;
	else if (this.direction==2)
		this.y-=this.speed;
	else if (this.direction==3)
		this.x-=this.speed;
	
	//out of screen
	if (this.x > CANVAS_WIDTH + 32 || this.x < -32 || this.y > CANVAS_HEIGHT+32 || this.y < -32)
	{
		this.destroy=true;
		return;
	}
	//hit obstacle
	for (var i=0;i<this.parent.instances.length;i++)
	{
		if( this.parent.instances[i] instanceof BlockInstance && this.isColliding(this.x,this.y,this.parent.instances[i]) )
		{
			this.destroy=true;
			break;
		}
		else if( this.parent.instances[i] instanceof EnemyInstance && this.parent.instances[i].state!=3 && this.isColliding(this.x,this.y,this.parent.instances[i]) )
		{
			//this.parent.instances[i].destroy=true;
			this.parent.instances[i].state=3;
			this.parent.instances[i].gravity=0.2;
			this.parent.instances[i].speedY=-3;
			this.parent.instances[i].speedX=getRandom(5)-2;
			this.destroy=true;
			this.parent.instances.push(new ExplosionInstance(this.parent,this.x,this.y));
			break;
		}
	}

}
