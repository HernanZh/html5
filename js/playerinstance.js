PlayerInstance.prototype = new Instance;
PlayerInstance.prototype.constructor = PlayerInstance;
function PlayerInstance(parent,x,y)
{
	this.parent=parent;
	this.gameParent=parent.parent;
	this.x=x;
	this.y=y;
	this.hitbox={'x':-12,'y':-24,'w':24,'h':24};
	this.setSprite(this.gameParent.sprites.playerStoppedDown);
	this.maxSpeed=2.0;
	this.direction=0; //0=down, 1=right,2=up,3=left
	this.state=0;
	this.distanceDiv=10.0;
	this.speedX=0;
	this.speedY=0;
	this.speedDelay=0;
	this.holdDelay=0;
	
	this.movePlayer=function()
	{
		//simple collision check in either direction
		//look ahead and see if its not colliding
		//could be done more precise by checking pixel by pixel, but lets keep it simple here!
		var isColliding=false;
		for (var i=0;i<this.parent.instances.length;i++)
		{
			if( (this.parent.instances[i] instanceof BlockInstance && this.isColliding(this.x,this.y+this.speedY,this.parent.instances[i])) 
				|| (this.parent.instances[i] instanceof BackgroundInstance && this.parent.instances[i].solid && this.isColliding(this.x,this.y+this.speedY,this.parent.instances[i])))
			{
				isColliding=true;
				break;
			}
		}
		if (!isColliding)
			this.y+=this.speedY;
		
		isColliding=false;
		for (var i=0;i<this.parent.instances.length;i++)
		{
			if( (this.parent.instances[i] instanceof BlockInstance && this.isColliding(this.x+this.speedX,this.y,this.parent.instances[i])) 
				|| (this.parent.instances[i] instanceof BackgroundInstance && this.parent.instances[i].solid && this.isColliding(this.x+this.speedX,this.y,this.parent.instances[i])))
			{
				isColliding=true;
				break;
			}
		}
		if (!isColliding)
			this.x+=this.speedX;
	}
	//limit the speed so the player doesnt run like crazy!
	this.limitSpeed=function()
	{
		this.speed=this.speedX*this.speedX+this.speedY*this.speedY;
		this.walkangle=Math.atan2(this.speedX,this.speedY);
		if (this.speed > this.maxSpeed*this.maxSpeed)
		{
			this.speedX=this.maxSpeed*Math.sin(this.walkangle);
			this.speedY=this.maxSpeed*Math.cos(this.walkangle);
			this.mouseX=this.startMouseX+this.speedX*this.distanceDiv;
			this.mouseY=this.startMouseY+this.speedY*this.distanceDiv;			
		}
	}
	this.shootBullet=function()
	{
		bullet=new BulletInstance(this.parent,this.x,this.y-16);
		this.parent.instances.push(bullet);
		if (this.direction==0)
			bullet.angle=0.5*Math.PI;
		else if (this.direction==1)
			bullet.angle=0;
		else if (this.direction==2)
			bullet.angle=-0.5*Math.PI;
		else if (this.direction==3)
			bullet.angle=Math.PI;
		bullet.direction=this.direction;
		
		this.parent.screenShake(10,4);
		//add smoke
		for (var i=0;i<2;i++)
		{
			//randomize direction
			var angle=bullet.angle+0.5*Math.PI*(this.direction%2?1:-1)+(Math.random()-0.5)*Math.PI;
			var speedX=Math.sin(angle);
			var speedY=Math.cos(angle);
			this.parent.instances.push(new SmokeInstance(this.parent,this.x,this.y-16,speedX,speedY,angle));
		}

	}
}

PlayerInstance.prototype.draw = function(context)
{
	if (this.gameParent.inputManager.mouseHold > 5)
	{
		//draw circle representing virtual stick
		context.fillStyle = "white";
		context.beginPath();
		var dist=10;
		context.arc(this.x+this.speedX*dist,this.y+this.speedY*dist,5,0,2*Math.PI);
		context.closePath();
		context.fill();
	}

	this.sprite.draw(context);
}

PlayerInstance.prototype.update = function()
{
	this.sprite.update();
	
	//moving: using the mouse as a virtual joystick
	if (this.gameParent.inputManager.mouseHold == 1)
	{
		//remember starting position
		this.mouseX=this.gameParent.inputManager.lastMouseX;
		this.mouseY=this.gameParent.inputManager.lastMouseY;
		this.startMouseX=this.gameParent.inputManager.lastMouseX;
		this.startMouseY=this.gameParent.inputManager.lastMouseY;
	}
	if (this.gameParent.inputManager.mouseHold > 1 && this.state==0)
	{
		//move by difference of mouse position
		this.mouseX+=this.gameParent.inputManager.deltaX;
		this.mouseY+=this.gameParent.inputManager.deltaY;
		this.speedX=(this.mouseX-this.startMouseX)/this.distanceDiv;
		this.speedY=(this.mouseY-this.startMouseY)/this.distanceDiv;
		this.limitSpeed();
		//direction
		if (this.speed > 0.01)
		{
			if ( this.walkangle >= -0.25*Math.PI && this.walkangle <= 0.25*Math.PI )
				this.direction=0;
			else if (this.walkangle >= 0.25*Math.PI && this.walkangle <= 0.75*Math.PI)
				this.direction=1;
			else if (this.walkangle >= -0.75*Math.PI && this.walkangle <= -0.25*Math.PI)
				this.direction=3;
			else
				this.direction=2;			
		}
		this.movePlayer();
	}
	else
	{
		//remember last speed
		this.speedDelay=this.speed;
		this.speed=0;
	}	
	
	//move using keyboard controls
	if (this.gameParent.inputManager.mouseHold==0)
	{
		if (this.gameParent.inputManager.up > 0)
		{
			this.speedY=-this.maxSpeed;
			this.direction=2;
		}
		else if (this.gameParent.inputManager.down > 0)
		{
			this.speedY=this.maxSpeed;
			this.direction=0;
		}
		else
		{
			this.speedY=0;
		}
		if (this.gameParent.inputManager.left > 0)
		{
			this.speedX=-this.maxSpeed;
			this.direction=3;
		}
		else if (this.gameParent.inputManager.right > 0)
		{
			this.speedX=this.maxSpeed;
			this.direction=1;
		}
		else
		{
			this.speedX=0;
		}

		if (this.gameParent.inputManager.up>0 || this.gameParent.inputManager.down>0 || this.gameParent.inputManager.left>0 || this.gameParent.inputManager.right>0)
		{
			this.limitSpeed();
			this.movePlayer();
		}
	}
	//shoot
	if (this.gameParent.inputManager.A==1)
	{
		this.gameParent.inputManager.A++;
		this.shootBullet();
	}
	//detect tap
	if (this.gameParent.inputManager.mouseHold > 0)
		this.holdDelay=this.gameParent.inputManager.mouseHold;
	
	if (this.gameParent.inputManager.mouseHold==0 && this.holdDelay>0 && this.holdDelay <10 && this.speedDelay < 0.001)
	{
		this.holdDelay=0;
		this.shootBullet();
	}
		
	if (this.gameParent.inputManager.mouseHold==0 && this.holdDelay>0)
		this.holdDelay=0;
	
	
	//set animation
	if (this.speed > 0 && this.state==0)
	{
		switch (this.direction)
		{
			case 0: this.setSprite(this.gameParent.sprites.playerWalkingDown); break;
			case 1: this.setSprite(this.gameParent.sprites.playerWalkingRight); break;
			case 2: this.setSprite(this.gameParent.sprites.playerWalkingUp); break;
			case 3: this.setSprite(this.gameParent.sprites.playerWalkingLeft); break;
		}
		this.imageSpeed=0.2;
	}
	else
	{
		switch (this.direction)
		{
			case 0: this.setSprite(this.gameParent.sprites.playerStoppedDown); break;
			case 1: this.setSprite(this.gameParent.sprites.playerStoppedRight); break;
			case 2: this.setSprite(this.gameParent.sprites.playerStoppedUp); break;
			case 3: this.setSprite(this.gameParent.sprites.playerStoppedLeft); break;
		}
	}
	
	//boundary
	if (this.x < 0)
		this.x=0;
	else if (this.x > CANVAS_WIDTH)
		this.x=CANVAS_WIDTH;
	if (this.y < 0)
		this.y=0;
	else if (this.y > CANVAS_HEIGHT)
		this.y=CANVAS_HEIGHT;

}