ExplosionInstance.prototype = new Instance;
ExplosionInstance.prototype.constructor = ExplosionInstance;
function ExplosionInstance(parent,x,y)
{
	this.parent=parent;
	this.gameParent=parent.parent;
	this.x=x;
	this.y=y;
	this.z=1;
	this.setSprite(this.gameParent.sprites.explosion);
	this.imageSpeed=0.2;
}
ExplosionInstance.prototype.animationEndCallback=function()
{
	this.destroy=true;
}
