function GetRelativePosition(canvas, x,y) 
{
	var x = x - canvas.offsetLeft;
	var y = y - canvas.offsetTop;

	return {"x": x, "y": y};
}

InputManager = function(parent,canvas)
{
	this.parent=parent;
	this.canvas=canvas;
	this.reset = function()
	{
		this.mouseDown = false;
		this.mouseHold = 0;
		this.deltaX = 0;
		this.deltaY = 0;
	}
	this.up=0;
	this.down=0;
	this.left=0;
	this.right=0;
	this.A=0;
	
    this.reset();
	this.lastMouseX = 0;
	this.lastMouseY = 0;
	
	this.update=function()
	{
		//mouseHold counter needs to be updated every tick
		if (this.mouseDown)
			this.mouseHold++;
		else
			this.mouseHold=0;
	}
	
	this.handleMouseDown = function (event)
	{
		this.mouseDown = true;
		var newPos = GetRelativePosition(this.canvas, event.pageX, event.pageY);
		this.lastMouseX = newPos.x/this.parent.scale;
		this.lastMouseY = newPos.y/this.parent.scale;
	}

	this.handleMouseUp = function (event)
	{
		this.mouseDown = false;
	}

	this.handleMouseMove = function (event)
	{
		var newPos = GetRelativePosition(this.canvas, event.pageX, event.pageY);

		this.deltaX = newPos.x/this.parent.scale - this.lastMouseX;
		this.deltaY = newPos.y/this.parent.scale - this.lastMouseY;

		this.lastMouseX = newPos.x/this.parent.scale;
		this.lastMouseY = newPos.y/this.parent.scale;
	}
	
	this.touchMove = function(event)
	{
		this.lastMouseX = (event.pageX - canvas.offsetLeft)/this.parent.scale;
		this.lastMouseY = (event.pageY - canvas.offsetTop)/this.parent.scale;
	}
	
	this.handleKeyDown = function (event)
	{
		switch(event.keyCode)
		{
			case 37: this.left++; break;
			case 38: this.up++; break;
			case 39: this.right++; break;
			case 40: this.down++; break;
			case 32: this.A++; break;
		}
	}

	this.handleKeyUp = function (event)
	{
		switch(event.keyCode)
		{
			case 37: this.left=0; break;
			case 38: this.up=0; break;
			case 39: this.right=0; break;
			case 40: this.down=0; break;
			case 32: this.A=0; break;
		}
	}
}
