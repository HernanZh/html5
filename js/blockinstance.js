BlockInstance.prototype = new Instance;
BlockInstance.prototype.constructor = BlockInstance;
function BlockInstance(parent,x,y)
{
	this.gameParent=parent.parent;
	this.x=x;
	this.y=y;
	this.hitbox={'x':0,'y':-32,'w':32,'h':32};
	this.setSprite(this.gameParent.sprites.block);
	this.maxSpeed=2.0;
	this.direction=0; //0=down, 1=right,2=up,3=left
}
