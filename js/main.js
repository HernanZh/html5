//global vars and functions
CANVAS_WIDTH=288;
CANVAS_HEIGHT=320;
CANVAS_RATIO=CANVAS_WIDTH/CANVAS_HEIGHT;

//game vars
highscore=0;

function sign(a)
{
	return a >= 0 ? 1:-1;
}

function getRandom(a)
{
	return Math.floor(Math.random()*a);
}

//requestanimationframe for mainloop
//http://www.playmycode.com/blog/2011/08/building-a-game-mainloop-in-javascript/
var animFrame = window.requestAnimationFrame || 
			window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame    ||
            window.oRequestAnimationFrame      ||
            window.msRequestAnimationFrame     ||
            null ;

//start
window.onload = function () 
{
	var currentWidth=320;
	var currentHeight=480;

	var useragent = navigator.userAgent.toLowerCase();
	var android = useragent.indexOf('android') > -1 ? true : false;
	var ios = ( useragent.indexOf('iphone') > -1 || useragent.indexOf('ipad') > -1  ) ? true : false;
	
	//create the canvas
    canvas = document.getElementById("game");
    ctx = canvas.getContext("2d");
	canvas.width = CANVAS_WIDTH;
	canvas.height = CANVAS_HEIGHT;
	//the constructor calls Game.init()
	game=new Game(canvas,ctx);
	
	//Events for controls
	window.addEventListener('mousedown', function(event) 
	{
		game.inputManager.handleMouseDown(event);
		event.preventDefault();
	}, false);
	window.addEventListener('mousemove', function(event) 
	{
		game.inputManager.handleMouseMove(event);
		event.preventDefault();
	}, false);
	window.addEventListener('mouseup', function(event) 
	{
		game.inputManager.handleMouseUp(event);
		event.preventDefault();
	}, false);
	// listen for touches
	window.addEventListener('touchstart', function(event) 
	{
		game.inputManager.handleMouseDown(event);
		event.preventDefault();
	}, false);
	window.addEventListener('touchmove', function(event) 
	{
		game.inputManager.handleMouseMove(event);
		event.preventDefault();
	}, false);
	window.addEventListener('touchend', function(event) 
	{
		game.inputManager.handleMouseUp(event);
		event.preventDefault();
	}, false);
	//keyboard controls
	window.addEventListener('keydown', function(event)
	{
		game.inputManager.handleKeyDown(event);
		event.preventDefault();	
	}, false );	
	window.addEventListener( 'keyup', function(event)
	{
		game.inputManager.handleKeyUp(event);
		event.preventDefault();	
	}, false );	
	
	//resize for mobile
	if (android || ios) 
	{
		currentHeight = window.innerHeight;
		currentWidth = currentHeight * CANVAS_RATIO;
		this.canvas.style.width = currentWidth + 'px';
		this.canvas.style.height = currentHeight + 'px';
		game.scale=currentWidth / CANVAS_WIDTH;
		//document.body.style.height = (window.innerHeight + 50) + 'px';
		//window.setTimeout(function() {window.scrollTo(0,1);}, 1);
	}
	else
	{
		//resize to 2x for better view on desktop
		currentHeight = CANVAS_WIDTH*2;
		currentWidth = currentHeight * CANVAS_RATIO;
		this.canvas.style.width = currentWidth + 'px';
		this.canvas.style.height = currentHeight + 'px';
		game.scale=currentWidth / CANVAS_WIDTH;
	}
};