//http://www.html5canvastutorials.com/tutorials/html5-canvas-image-loader/
function loadImages(sources, callback) 
{
	var images = {};
	var loadedImages = 0;
	var numImages = 0;
	// get num of sources
	for(var src in sources) 
	{
	  numImages++;
	}
	for(var src in sources) 
	{
		images[src] = new Image();
		images[src].onload = function() 
		{
			if(++loadedImages >= numImages) 
			{
				callback(images);
			}
		};
		images[src].src = sources[src];
	}
}
//this is the mainloop using requestAnimationFrame, if not available, it will use setInterval
function runGame()
{
	if ( animFrame !== null ) 
	{
		var recursiveAnim = function() 
		{
			game.mainloop();
			animFrame( recursiveAnim, canvas );
		}
		// start the mainloop
		animFrame( recursiveAnim, canvas );
	} 
	else 
	{
		var ONE_FRAME_TIME = 1000.0 / 60.0 ;
		setInterval( function(){game.mainloop()}, ONE_FRAME_TIME );
	}
}
			
//Game class
//The game object is the object that holds the mainloop functions
//and manages the game state (aka scenes)
var Game=function(canvas,ctx)
{
	this.canvas=canvas;
	this.context=ctx;
	this.scale=1;
	this.timer=0;
	this.inputManager=new InputManager(this,canvas);
	this.sprites={};
	this.images={};
	this.loading=true;	
	
	this.currentScene;
	this.nextScene;
	//When a level/room needs to end, call endScene(int nextscene)
	this.endScene=function(n)
	{
		this.currentScene.run=false;
		this.nextScene=n;
	}
	this.startScene=function(n)
	{
		if (this.currentScene)
			alert(this.currentScene.instances.length);
		switch (n)
		{
			case 0:
				this.currentScene=new TitleScene(this); break;
			case 1:
				this.currentScene=new MainScene(this); break;
			default:
				this.currentScene=new MainScene(this); break;
		}
	}
	
	//init
	this.prepareGame=function()
	{
		//define all sprites (animations)
		//note: same as animation, load this from external files
		this.sprites['bunnyWalking']=new Sprite(this.images.bunny,
		[[0,32,32,32],[32,32,32,32],[64,32,32,32],[96,32,32,32],[0,64,32,32],[32,64,32,32],[64,64,32,32],[96,64,32,32]],
		{'x':16,'y':32});
		this.sprites['bunnyStopped']=new Sprite(this.images.bunny,
		[[0,0,32,32]],
		{'x':16,'y':32});
		this.sprites['playerStoppedDown']=new Sprite(this.images.girl,
		[[0,0,32,40]],
		{'x':16,'y':40});
		this.sprites['playerStoppedRight']=new Sprite(this.images.girl,
		[[32,0,32,40]],
		{'x':16,'y':40});
		this.sprites['playerStoppedUp']=new Sprite(this.images.girl,
		[[64,0,32,40]],
		{'x':16,'y':40});
		this.sprites['playerStoppedLeft']=new Sprite(this.images.girl,
		[[96,0,32,40]],
		{'x':16,'y':40});
		this.sprites['playerWalkingDown']=new Sprite(this.images.girl,
		[[0,40,32,40],[32,40,32,40],[64,40,32,40],[96,40,32,40],[128,40,32,40],[160,40,32,40],[192,40,32,40],[224,40,32,40],],
		{'x':16,'y':40});
		this.sprites['playerWalkingRight']=new Sprite(this.images.girl,
		[[0,80,32,40],[32,80,32,40],[64,80,32,40],[96,80,32,40],[128,80,32,40],[160,80,32,40],[192,80,32,40],[224,80,32,40],],
		{'x':16,'y':40});
		this.sprites['playerWalkingUp']=new Sprite(this.images.girl,
		[[0,120,32,40],[32,120,32,40],[64,120,32,40],[96,120,32,40],[128,120,32,40],[160,120,32,40],[192,120,32,40],[224,120,32,40],],
		{'x':16,'y':40});
		this.sprites['playerWalkingLeft']=new Sprite(this.images.girl,
		[[0,160,32,40],[32,160,32,40],[64,160,32,40],[96,160,32,40],[128,160,32,40],[160,160,32,40],[192,160,32,40],[224,160,32,40],],
		{'x':16,'y':40});
		this.sprites['block']=new Sprite(this.images.block,
		[[0,0,32,40]],
		{'x':0,'y':40});
		this.sprites['button']=new Sprite(this.images.button,
		[[0,0,200,40]],
		{'x':0,'y':0});
		this.sprites['monsterAppear']=new Sprite(this.images.monster,
		[[0,36,24,24],[24,36,24,24],[48,36,24,24],[72,36,24,24],[0,36+24,24,24],[24,36+24,24,24],[48,36+24,24,24],[72,36+24,24,24],[0,36+48,24,24],[0,36+48,24,24]],
		{'x':13,'y':22});
		this.sprites['monsterStopped']=new Sprite(this.images.monster,
		[[0,0,24,18],[0,0,24,18],[0,0,24,18],[0,0,24,18],[0,0,24,18],[24,0,24,18],[48,0,24,18],[24,0,24,18],[0,0,24,18],
		[72,0,24,18],[96,0,24,18],[72,0,24,18],],
		{'x':12,'y':16});
		this.sprites['monsterWalking']=new Sprite(this.images.monster,
		[[0,18,24,18],[24,18,24,18],],
		{'x':12,'y':16});
		this.sprites['monsterDead']=new Sprite(this.images.monster,
		[[24,36+48,24,24]],
		{'x':12,'y':16});
		this.sprites['bullet']=new Sprite(this.images.bullet,
		[[0,0,22,7]],
		{'x':11,'y':3});
		this.sprites['smoke']=new Sprite(this.images.fire,
		[[0,64,32,32],[32,64,32,32],[64,64,32,32],[0,96,32,32],[32,96,32,32],[64,96,32,32]],
		{'x':16,'y':16});
		this.sprites['explosion']=new Sprite(this.images.fire,
		[[0,0,32,32],[32,0,32,32],[64,0,32,32],[96,0,32,32],[0,32,32,32],[32,32,32,32]],
		{'x':16,'y':16});

		//create the first scene
		this.startScene(0);
	}
	this.init=function()
	{
		if (this.loading)
		{
			//show  aloading screen
			this.context.fillStyle = "black";
			this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
			this.context.fillStyle = "white";
			this.context.fillText("Loading game...", 10,25);
		}
		//load all assets
		//note: it would be cleaner to load a list from an external file, but considering the scope of this project this is done manually
		var sources = 
		{
			monster: 'assets/monster.png',
			ground: 'assets/ground.png',
			girl: 'assets/girl.png',
			fire: 'assets/fire.png',
			bullet: 'assets/bullet.png',
			block: 'assets/block.png',
			button: 'assets/button.png',
			bunny: 'assets/bunny.png'
		};
		var game=this;
		loadImages(sources,
		function(images)
		{
			game.loading=false;
			game.images=images;
			//init other things after images are loaded
			game.prepareGame();
			runGame();
		});
	}
	this.init();

	//update
	this.updateGame=function()
	{
		if (this.loading)
			return;
			
		this.timer+=1;
		//update input manager
		this.inputManager.update();
		
		//update scene
		if (this.currentScene)
		{
			if (this.currentScene.run)
				this.currentScene.update();
			else
			{
				//clean up and start new scene
				this.currentScene.end();
				this.currentScene=null;
				this.startScene(this.nextScene);
			}
		}
		//reset mouse position difference at the end
		this.inputManager.deltaX = 0;
		this.inputManager.deltaY = 0;
	}
	
	//render
	this.drawGame=function()
	{
		this.context.fillStyle = "#0E0E14";
		this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
				
		if (this.currentScene)
		{
			this.currentScene.draw(this.context);
		}
		
		//debug
		//this.context.fillStyle = "black";
		//this.context.fillText("Touch and drag the ball", 10,25);
		
		//this.context.fillStyle = "red";
		//this.context.fillText("frame: "+this.timer, 10,25);
		//this.context.fillText("mouse: "+this.inputManager.lastMouseX+", " +this.inputManager.lastMouseY, 10,35);
		//this.context.fillText("down "+this.inputManager.mouseHold, 10,45);
	}

	//main loop
	this.mainloop=function() 
	{
		this.updateGame();
		this.drawGame();
	}
}