EnemyInstance.prototype = new Instance;
EnemyInstance.prototype.constructor = EnemyInstance;
function EnemyInstance(parent,x,y)
{
	this.parent=parent;
	this.gameParent=parent.parent;
	this.x=x;
	this.y=y;
	this.hitbox={'x':-12,'y':-12,'w':24,'h':24};
	this.setSprite(this.gameParent.sprites.monsterAppear);
	this.maxSpeed=2.0;
	this.state=0;
	this.stateTimer=0;
	this.stateDuration=45;
	this.speedX=0;
	this.speedY=0;
	this.gravity=0;
}

EnemyInstance.prototype.update = function()
{
	this.sprite.update();
	
	this.stateTimer++;
	if (this.state==0)
	{
		//appearing
		this.imageSpeed=0.2;
		if (this.stateTimer>this.stateDuration)
		{
			this.state=1;
			this.stateTimer=0;
			this.stateDuration=getRandom(60)+60;
		}
	}
	else if (this.state==1)
	{
		//stopped
		this.setSprite(this.gameParent.sprites.monsterStopped);
		this.imageSpeed=0.1;
		if (this.stateTimer>this.stateDuration)
		{
			this.state=2;//walk around
			this.stateTimer=0;
			this.stateDuration=getRandom(60)+60;
			this.speedX=Math.random()*1;
			this.speedY=Math.random()*1;
		}

	}
	else if (this.state==2)
	{
		//walking
		this.setSprite(this.gameParent.sprites.monsterWalking);
		this.imageSpeed=0.1;
		if (this.stateTimer>this.stateDuration)
		{
			this.state=1;//stop
			this.stateTimer=0;
			this.stateDuration=getRandom(60)+60;
		}
		
		var isColliding=false;
		for (var i=0;i<this.parent.instances.length;i++)
		{
			if( (this.parent.instances[i] instanceof BlockInstance && this.isColliding(this.x,this.y+this.speedY,this.parent.instances[i])) 
				|| (this.parent.instances[i] instanceof BackgroundInstance && this.parent.instances[i].solid && this.isColliding(this.x,this.y+this.speedY,this.parent.instances[i])))
			{
				isColliding=true;
				this.speedY=this.speedY*-1;
				break;
			}
		}
		if (!isColliding)
			this.y+=this.speedY;
		
		isColliding=false;
		for (var i=0;i<this.parent.instances.length;i++)
		{
			if( (this.parent.instances[i] instanceof BlockInstance && this.isColliding(this.x+this.speedX,this.y,this.parent.instances[i])) 
				|| (this.parent.instances[i] instanceof BackgroundInstance && this.parent.instances[i].solid && this.isColliding(this.x+this.speedX,this.y,this.parent.instances[i])))
			{
				isColliding=true;
				this.speedX=this.speedX*-1;
				break;
			}
		}
		if (!isColliding)
			this.x+=this.speedX;
		
	}
	else if (this.state==3)
	{
		//dead: fly off
		this.speedY+=this.gravity;
		this.angle+=0.2;
		this.z=1;
		this.setSprite(this.gameParent.sprites.monsterDead);
		this.x+=this.speedX;
		this.y+=this.speedY;
		
		if (this.y>CANVAS_HEIGHT)
		{
			this.destroy=true;
		}
	}
	
	if (this.state!=3)
		this.parent.clearTime++;

}